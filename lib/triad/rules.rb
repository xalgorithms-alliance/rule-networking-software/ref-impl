module Triad
  module Rules
    class Condition
      attr_reader(:expr)
      attr_reader(:scenarios)

      def initialize(expr, scenarios)
        @expr = expr
        @scenarios = scenarios
      end

      def self.from_h(h)
        Condition.new(Expressions::Expr.from_h(h['expression']), h['scenarios'])
      end
      
      def to_h
        {
          expression: @expr.to_h,
          scenarios: @scenarios,
        }
      end
    end

    class Assertion
      attr_reader(:key)
      attr_reader(:value)
      attr_reader(:scenarios)

      def initialize(key, val, scenarios)
        @key = key
        @value = val
        @scenarios = scenarios
      end

      def self.from_h(h)
        Assertion.new(h['key'], h['value'], h['scenarios'])
      end

      def to_h
        {
          key: @key,
          value: @value,
          scenarios: @scenarios,
        }
      end
    end

    class Rule
      attr_reader(:in_effect)
      attr_reader(:conditions)
      attr_reader(:assertions)
      attr_reader(:properties)

      MAYBE_PARSE_DATE = ->(v) do
        return Time.parse(v) if v.kind_of?(String)

        v
      end

      CONVERT_FNS = {
        :from => MAYBE_PARSE_DATE,
        :to   => MAYBE_PARSE_DATE,
      }

      def initialize(in_effect, conditions, assertions, properties)
        @in_effect = in_effect.map do |tup|
          tup.reduce({}) { |h, (k, v)| h.merge(k => CONVERT_FNS.fetch(k, ->(v) { v }).call(v)) }
        end
        @conditions = conditions
        @assertions = assertions
        @properties = properties
      end

      def id
        @properties[:id]
      end

      def self.from_key_parsers(h, kps)
        kps.keys.reduce({}) do |oh, k|
          oh.merge(k => h[k.to_s])
        end
      end

      def self.from_h(h)
        Rule.new(
          h['in_effect'].map do |ie_h|
            from_key_parsers(ie_h, Triad::Parse::IN_EFFECT_KEY_PARSERS)
          end,
          h['input_conditions'].map do |ic_h|
            Rules::Condition.from_h(ic_h)
          end,
          h['output_assertions'].map do |oa_h|
            Rules::Assertion.from_h(oa_h)
          end,
          from_key_parsers(h['properties'], Triad::Parse::PROPERTIES_KEY_PARSERS)
        )
      end

      def to_h
        {
          in_effect: @in_effect.map do |tup|
            tup.merge(from: tup[:from].iso8601, to: tup[:to].iso8601)
          end,
          properties: @properties,
          input_conditions: @conditions.map(&:to_h),
          output_assertions: @assertions.map(&:to_h),
        }
      end
    end
  end
end
