module Triad
  module Expressions
    class Expr; end

    class Invalid < Expr
      def initialize
        super(nil, nil)
      end
    end

    class Equals < Expr
      def eval(doc)
        doc.fetch(key, nil) == value
      end

      def to_h
        super().merge(op: 'eq')
      end
    end

    class Expr
      OPS = {
        '=' => Equals,
      }

      def self.parse(s)
        m = /([a-zA-Z][a-zA-Z0-9_]*)([\=])(.+)/.match(s)
        if m
          (k, op, v) = m[1..3]
          return OPS.fetch(op, Invalid).new(k, Val.parse(v))
        end
      end

      attr_reader(:key)
      attr_reader(:value)

      def initialize(k, v)
        @key = k
        @value = v
      end

      def eval(v)
        false
      end

      def self.from_h(h)
        return Invalid.new unless h

        case h['op']
        when 'eq'
          Equals.new(h['key'], h['value'])
        end
      end

      def to_h
        { key: @key, value: @value }
      end
    end
  end
end
