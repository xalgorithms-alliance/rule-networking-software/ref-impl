require_relative('../../lib/triad')

RSpec.describe(Triad::Invoke) do
  it 'should generate no oughts with no input' do
    oughts = Triad::Invoke.call({}, [], [])
    expect(oughts).to be_empty
  end

  it 'should yield assertion for a matching condition' do
    cond = Triad::Rules::Condition.new(
      Triad::Expressions::Equals.new('cond_k', 1),
      ['01']
    )
    asrt = Triad::Rules::Assertion.new('asrt_k', 'asrt_v', ['11'])
    doc = {
      'cond_k' => 1,
    }

    ex = [
      [{ k: 'asrt_k', v: 'asrt_v', sc: '11'}],
    ]

    oughts = Triad::Invoke.call(doc, [cond], [asrt])
    expect(oughts).to eql(ex)
  end

  it 'should yield only matching conditions' do
    # first and third scenarios match
    conds = [
      Triad::Rules::Condition.new(
        Triad::Expressions::Equals.new('cond_k', 1),
        ['01', '00', '01']),
      # negation
      Triad::Rules::Condition.new(
        Triad::Expressions::Equals.new('cond_k', 2),
        ['00', '01', '00']),
    ]
    asrts = [
      Triad::Rules::Assertion.new('asrt_k', 'asrt_v', ['11', '10', '01']),
      Triad::Rules::Assertion.new('asrt_k_neg', 'asrt_v_neg', ['00', '11', '11']),
    ]
    doc = {
      'cond_k' => 1,
    }

    ex = [
      # first
      [
        { k: 'asrt_k', v: 'asrt_v', sc: '11'},
        {:k=>"asrt_k_neg", :sc=>"00", :v=>"asrt_v_neg"},
      ],
      # third
      [
        { k: 'asrt_k', v: 'asrt_v', sc: '01'},
        {:k=>"asrt_k_neg", :sc=>"11", :v=>"asrt_v_neg"},
      ],
    ]

    oughts = Triad::Invoke.call(doc, conds, asrts)
    expect(oughts).to eql(ex)
  end

  it 'should satisfy a collection of pre-written cases' do
    require('multi_json')
    require('pathname')

    tests = Dir[File.join('fixtures', '*')].reduce({}) do |h, path|
      name = File.basename(path)
      (doc_paths, ex_paths) = Dir[File.join(path, '*.json')].partition { |fn| fn.include?('.doc.') }
      tests = doc_paths.zip(ex_paths).map do |(doc_path, ex_path)|
        { doc: MultiJson.load(File.read(doc_path)), ex: MultiJson.load(File.read(ex_path)) }
      end
      h.merge(name => {
                rule: Triad::Parse.parse_file(File.join(path, "#{name}.ior")),
                tests: tests,
              })
    end

    tests.each do |name, o|
      o[:tests].each_with_index do |t, i|
        ac = Triad::Invoke.call(t[:doc], o[:rule].conditions, o[:rule].assertions)
        expect(MultiJson.load(ac.to_json)).to(eql(t[:ex]), "wrong result for #{name}/#{i}")
      end
    end
  end
end
