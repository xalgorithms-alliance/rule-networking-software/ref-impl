ENV['APP_ENV'] = 'test'

require('rack/test')

require_relative('../../lib/triad')
require_relative('../../lib/triad/api')
require_relative('./doc_gen')

RSpec.describe(Triad::API) do
  include Rack::Test::Methods
  include DocGen

  def app
    Triad::API
  end

  after(:each) do
    FileUtils.rm_rf('contexts')
  end

  it 'GET / reports the version' do
    get('/')

    expect(last_response).to be_ok
    expect(last_response.body).to eq({ version: '0.0.0'}.to_json)
  end

  it 'POST /contexts creates a context' do
    context = { foo: 'a', bar: 'b' }
    post('/contexts', context.to_json)

    expect(last_response).to be_ok

    o = JSON.parse(last_response.body)
    expect(o).to have_key('id')

    fn = File.join('contexts', o['id'], 'context.json')
    expect(File.exist?(fn)).to be_truthy
    expect(File.read(fn)).to eq(context.to_json)
  end

  it 'POST /contexts fails when data is not JSON' do
    post('/contexts', '{ asdasdasd }')

    expect(last_response).to_not be_ok
    expect(last_response.status).to eq(400)
    expect(last_response.body).to eq({ reason: :requires_json }.to_json)
  end

  it 'POST /contexts/:id/documents stores the document' do
    context_id = SecureRandom.uuid
    context_dn = File.join('contexts', context_id)

    FileUtils.mkdir_p(context_dn)

    doc = generate_document

    post("/contexts/#{context_id}", doc.to_json)

    sub_dn = File.join(context_dn, 'submissions')
    expect(File.exist?(sub_dn)).to be_truthy

    sub_fn = Dir[File.join(sub_dn, '*')].first
    expect(sub_fn).to_not be_nil
    expect(File.read(sub_fn)).to eq(doc.to_json)

    sub_id = File.basename(sub_fn, '.json')
    resp = JSON.parse(last_response.body)
    expect(resp).to have_key('id')
    expect(resp['id']).to eq(sub_id)
  end

  it 'POST /contexts/:id fails when data is not JSON' do
    post('/contexts/12345', '{ asdasdasd }')

    expect(last_response).to_not be_ok
    expect(last_response.status).to eq(400)
    expect(last_response.body).to eq({ reason: :requires_json }.to_json)

    expect(File.exist?(File.join('contexts', '12345'))).to be_falsey
  end

  it 'POST /contexts/:id fails when context does not exist' do
    post('/contexts/12345', '{ "a" : "1" }')

    expect(last_response).to_not be_ok
    expect(last_response.status).to eq(403)
    expect(last_response.body).to eq({ reason: :context_not_found }.to_json)

    expect(File.exist?(File.join('contexts', '12345'))).to be_falsey
  end
end
