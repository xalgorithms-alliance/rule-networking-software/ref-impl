require('faker')
require('fileutils')
require('json')
require('tmpdir')

require_relative('../../lib/triad')
require_relative('./rule_gen')

RSpec.describe(Triad::Sessions) do
  include RuleGen

  it 'should make space for session data' do
    Dir.mktmpdir do |dir|
      sess = Triad::Sessions::Session.make(dir)
      expect(sess.id).to_not be_nil
      expect(sess.rules).to be_empty
      expect(File.exist?("#{dir}/#{sess.id}")).to be(true)
    end
  end

  it 'should list sessions and contents' do
    session_dirs = Faker::Number.within(range: 3..8).times.map do
      {
        id: Faker::Internet.uuid,
        rules: Faker::Number.within(range: 3..8).times.map { Faker::Internet.uuid },
      }
    end.sort_by { |sess_dir| [sess_dir[:id]] }

    Dir.mktmpdir do |dir|
      # build the
      session_dirs.each do |sess_dir|
        sess_path = File.join(dir, sess_dir[:id])
        FileUtils.mkdir_p(sess_path)
        sess_dir[:rules].each { |id| FileUtils.mkdir_p(File.join(sess_path, id)) }
      end

      ss = Triad::Sessions.list(dir).sort_by { |sess| [sess.id] }

      expect(ss.count).to eql(session_dirs.count)

      ss.zip(session_dirs).each do |sess, sess_dir|
        expect(sess.id).to eql(sess_dir[:id])
        expect(sess.path).to eq(File.join(dir, sess_dir[:id]))

        expect(sess.rules.count).to eql(sess_dir[:rules].count)
        sess.rules.sort_by { |r| [r.id] }.zip(sess_dir[:rules].sort).each do |sess_rule, rule_id|
          expect(sess_rule.id).to eql(rule_id)
        end
      end
    end
  end

  it 'should load a session' do
    session_dirs = Faker::Number.within(range: 3..8).times.map do
      {
        id: Faker::Internet.uuid,
        rules: Faker::Number.within(range: 3..8).times.map { Faker::Internet.uuid },
      }
    end.sort_by { |sess_dir| [sess_dir[:id]] }

    Dir.mktmpdir do |dir|
      # build the
      session_dirs.each do |sess_dir|
        sess_path = File.join(dir, sess_dir[:id])
        FileUtils.mkdir_p(sess_path)
        sess_dir[:rules].each { |id| FileUtils.mkdir_p(File.join(sess_path, id)) }

        sess = Triad::Sessions.load(sess_dir[:id], dir)

        expect(sess).to_not be_nil
        expect(sess.id).to eql(sess_dir[:id])
        expect(sess.path).to eq(File.join(dir, sess_dir[:id]))

        expect(sess.rules.count).to eql(sess_dir[:rules].count)
        sess.rules.sort_by { |r| [r.id] }.zip(sess_dir[:rules].sort).each do |sess_rule, rule_id|
          expect(sess_rule.id).to eql(rule_id)
        end
      end
    end
  end

  it 'should load copies of rules' do
    sess_id = Faker::Internet.uuid
    rules = Faker::Number.within(range: 2..4).times.map do
      {
        id: Faker::Internet.uuid,
        copies: Faker::Number.within(range: 2..4).times.reduce([]) do |a, _|
          t = a.last ? a.last[:time] : Faker::Time.backward(days: 1)
          a << {
            content: generate_rule.to_h,
            time: t + Faker::Number.within(range: 10..100).minutes,
          }
        end,
      }
    end

    Dir.mktmpdir do |dir|
      rules.each_with_index do |rule, i|
        rule_path = File.join(dir, sess_id, rule[:id])
        FileUtils.mkdir_p(rule_path)
        rule[:copies].each do |cp|
          File.write(File.join(rule_path, "#{cp[:time].to_i}.json"), cp[:content].to_json)
        end
      end

      sess = Triad::Sessions.load(sess_id, dir)
      expect(sess.rules.count).to eql(rules.count)

      rules.sort_by { |r| [r[:id]] }.zip(sess.rules.sort_by { |r| [r.id] }).each do |(ex_rule, ac_rule)|
        expect(ex_rule[:id]).to eql(ac_rule.id)
        expect(ex_rule[:copies].count).to eql(ac_rule.copies.count)

        ex_rule[:copies].sort_by { |cp| [cp[:time]] }.zip(ac_rule.copies.sort_by { |cp| cp.time }).each do |(ex, ac)|
          expect(ex[:time]).to eql(ac.time)
          expect(ex[:content].with_indifferent_access).to eql(ac.content)
        end
      end
    end
  end

  it 'should automatically save files' do
    # two sessions, one non-existent, one existent
    sessions = { }

    Dir.mktmpdir do |dir|
      2.times.each do
        sess = Triad::Sessions::Session.make(dir)
        sessions[sess.id] = []
        Faker::Number.within(range: 2..4).times.each do
          rule = Triad::Sessions::Rule.make(sess.path)
          copy_ids = Faker::Number.within(range: 2..4).times.map do
            rule.add(generate_rule.to_h)
          end
          expect(copy_ids.sort).to eql(rule.copies.map { |cp| cp.time.to_i }.sort)
          sessions[sess.id] = (sessions[sess.id] + [{ id: rule.id, copies: rule.copies}]).sort_by { |r| r[:id] }
        end
      end

      sessions.each do |id, ex_rules|
        ac_sess = Triad::Sessions.load(id, dir)
        expect(ac_sess).to_not be_nil

        ac_rules = ac_sess.rules.sort_by { |r| [r.id] }
        expect(ac_rules.count).to eql(ex_rules.count)
        ex_rules.zip(ac_rules).each do |(ex_rule, ac_rule)|
          expect(ac_rule.id).to eql(ex_rule[:id])

          ex_copies = ex_rule[:copies].sort_by { |cp| cp.time }
          ac_copies = ac_rule.copies.sort_by { |cp| cp.time }
          expect(ac_copies.count).to eql(ex_copies.count)
          ex_copies.zip(ac_copies).each do |(ex_cp, ac_cp)|
            expect(ac_cp.time).to eql(ex_cp.time)
            expect(ac_cp.content).to eql(ex_cp.content)
          end
        end
      end
    end
  end
end
