require('faker')

module DocGen
  def generate_document
    Faker::Number.within(range: 3..8).times.reduce({}) do |h, _|
      h.merge(Faker::Lorem.word => Faker::Lorem.word)
    end
  end
end
